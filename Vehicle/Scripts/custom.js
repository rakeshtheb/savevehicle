﻿$(document).ready(function () {
    $("#Vehicle_Types").on("change", function () {
        if ($("#Vehicle_Types Option:Selected").val() != 0) {
            $.ajax(
                {
                    url: '/Vehicles/Index?id=' + $("#Vehicle_Types Option:Selected").val(),
                    type: 'GET',
                    data: "",
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        window.location.href = data;
                    },
                    error: function () {
                        alert("error");
                    }
                });
        }

    });

});