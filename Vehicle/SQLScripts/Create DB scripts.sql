USE [master]
GO

/****** Object:  Database [Vehicle]    Script Date: 20/05/2019 10:04:05 PM ******/
CREATE DATABASE [Vehicle]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Vehicle', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Vehicle.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Vehicle_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Vehicle_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Vehicle].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Vehicle] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [Vehicle] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [Vehicle] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [Vehicle] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [Vehicle] SET ARITHABORT OFF 
GO

ALTER DATABASE [Vehicle] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [Vehicle] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [Vehicle] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [Vehicle] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [Vehicle] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [Vehicle] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [Vehicle] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [Vehicle] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [Vehicle] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [Vehicle] SET  DISABLE_BROKER 
GO

ALTER DATABASE [Vehicle] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [Vehicle] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [Vehicle] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [Vehicle] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [Vehicle] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [Vehicle] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [Vehicle] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [Vehicle] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [Vehicle] SET  MULTI_USER 
GO

ALTER DATABASE [Vehicle] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [Vehicle] SET DB_CHAINING OFF 
GO

ALTER DATABASE [Vehicle] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [Vehicle] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [Vehicle] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [Vehicle] SET QUERY_STORE = OFF
GO

ALTER DATABASE [Vehicle] SET  READ_WRITE 
GO



USE [Vehicle]
GO

/****** Object:  Table [dbo].[Vehicle]    Script Date: 20/05/2019 10:03:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Vehicle](
	[VehicleID] [bigint] NOT NULL,
	[VehicleType] [bigint] NULL,
	[Make] [nvarchar](100) NULL,
	[Model] [nvarchar](100) NULL,
	[Engine] [nvarchar](100) NULL,
 CONSTRAINT [PK_Vehicle] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



USE [Vehicle]
GO

/****** Object:  Table [dbo].[Car]    Script Date: 20/05/2019 10:02:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Car](
	[VehicleID] [bigint] NOT NULL,
	[Doors] [int] NULL,
	[Wheels] [int] NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_Car] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [FK_Car_Vehicle] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[Vehicle] ([VehicleID])
GO

ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [FK_Car_Vehicle]
GO


USE [Vehicle]
GO

/****** Object:  Table [dbo].[VehicleType]    Script Date: 20/05/2019 10:04:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VehicleType](
	[TypeID] [bigint] NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [Vehicle]
GO

/****** Object:  View [dbo].[V_VehicleType]    Script Date: 20/05/2019 10:05:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_VehicleType]
AS
SELECT        TypeID, 'Create ' + Name AS name
FROM            dbo.VehicleType
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "VehicleType"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VehicleType'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VehicleType'
GO



USE [Vehicle]
GO

INSERT INTO [dbo].[VehicleType]
           ([TypeID]
           ,[Name])
     VALUES
           (1
           ,'�ar')
GO


