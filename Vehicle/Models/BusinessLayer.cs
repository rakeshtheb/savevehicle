﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vehicle.Models;
using System.Data;
using System.Data.Entity;

namespace Vehicle.Models
{
    public class BusinessLayer
    {
        private VehicleEntities db = new VehicleEntities();
        public List<V_VehicleType> GetV_VehicleTypes()
        {
            //returns the list of vehicle types
            return db.V_VehicleType.ToList();
        }

        public void Add_Car(Car car)
        {
            
            //Function to add a car to the databse
            var c_type = db.VehicleTypes.Where(o => o.Name.Equals("Car")).FirstOrDefault();
            if (c_type != null)
            {
                long v_max = 0;
                var v_count = db.Vehicles.ToList().Count();
                if(v_count!=0)
                {
                    v_max = db.Vehicles.Max(o => o.VehicleID);
                }
                v_max++;
                car.VehicleID = v_max;
                car.VehicleType = c_type.TypeID;

                db.Vehicles.Add(car);
                db.SaveChanges();
             }
        }

        public Vehicle GetVehicle(long? vID)
        {
            //returns a vehilce with vehicleID  id
            return db.Vehicles.Where(o => o.VehicleID == vID).FirstOrDefault();
        }
    }
}