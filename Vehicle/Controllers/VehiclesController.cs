﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Vehicle.Models;

namespace Vehicle.Controllers
{

    public class VehiclesController : Controller
    {
       

        // GET: Vehicles
        public ActionResult Index(int id)
        {
            try
            {
                if (id == 1)
                {
                    return Content("/CarsVehicle/Create");
                }
                else
                {
                    return Content("/VehicleTypes/Index");
                }
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            
            
        }

        
    }
}
