﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Vehicle.Models;

namespace Vehicle.Controllers
{
    public class VehicleTypesController : Controller
    {
        private BusinessLayer bl = new BusinessLayer();

        // GET: VehicleTypes
        public ActionResult Index()
        {
            try
            {
                var types = bl.GetV_VehicleTypes();

                return View(types);
            }
            catch(Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

    }
}
