﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Vehicle.Models;

namespace Vehicle.Controllers
{
    public class CarsVehicleController : Controller
    {
        private BusinessLayer bl = new BusinessLayer();
       


        // GET: CarsVehicle/Details/5
        public ActionResult Details(long? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var vehicle = bl.GetVehicle(id);
                if (vehicle == null)
                {
                    return HttpNotFound();
                }
                return View(vehicle);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


        // GET: CarsVehicle/Create
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        // POST: CarsVehicle/Create
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VehicleID,VehicleType,Make,Model,Engine,Doors,Wheels,Type")] Car car)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    bl.Add_Car(car);
                    return RedirectToAction("Details?id="+car.VehicleID);
                }

                return View(car);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


    }
}
